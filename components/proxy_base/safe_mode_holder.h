#pragma once
#include "esphome/components/safe_mode/safe_mode.h"

namespace esphome
{
    namespace proxy_base
    {

        class SaveModeHolder
        {
        private:
            safe_mode::SafeModeComponent *safe_mode_;

        public:
            void set_safe_mode(safe_mode::SafeModeComponent *safe_mode) { safe_mode_ = safe_mode; }
            safe_mode::SafeModeComponent *get_safe_mode() { return safe_mode_; }
        };
    }
}
