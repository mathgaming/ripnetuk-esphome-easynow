#pragma once

#include "esphome/core/component.h"
#include "../proxy_base/proxy_base.h"
#include "peer_receiver.h"
#include "../proxy_base/log_tag.h"
#include <vector>
#include "sensor_holder.h"
#include "../proxy_base/wifi_holder.h"
#include "../proxy_base/peer_state.h"

namespace esphome
{
  namespace proxy_transmitter
  {
    class ProxyTransmitterComponent : public proxy_base::ProxyBaseComponent, public proxy_base::WiFiHolder
    {
    private:
      int send_complete = false;
    protected:
      proxy_base::LogTag *TAG = new proxy_base::LogTag("PeerReceiver");
      CallbackManager<void()> on_send_finished_callback_;

    public:
      PeerReceiver *peer_receiver_ = new PeerReceiver();
      void set_peer_receiver(PeerReceiver *peer_receiver) { peer_receiver_ = peer_receiver; }
      void add_on_send_finished_callback(std::function<void()> callback) { this->on_send_finished_callback_.add(std::move(callback)); }
      // PROCESSOR priority so we can kill off the wifi component before it starts...
      float get_setup_priority() const override { return setup_priority::PROCESSOR; }
      void proxy_loop();
      void proxy_setup();
    };
  } // namespace proxy_transmitter
} // namespace esphome
