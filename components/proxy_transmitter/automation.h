#pragma once

#include "esphome/core/automation.h"
#include "proxy_transmitter.h"

namespace esphome
{
  namespace proxy_transmitter
  {
    class SendFinishedTrigger : public Trigger<>
    {
    public:
      explicit SendFinishedTrigger(ProxyTransmitterComponent *transmitter)
      {
        transmitter->add_on_send_finished_callback([this](){ this->trigger(); });
      }
    };

    template <typename... Ts>
    class SendDataAction : public Action<Ts...>
    {
    public:
      explicit SendDataAction(ProxyTransmitterComponent *transmitter) : transmitter_(transmitter) {}

      void play(Ts... x) override { this->transmitter_->peer_receiver_->start_sensor_reads(); }
    protected:
      ProxyTransmitterComponent *transmitter_;
    };
  }
}
